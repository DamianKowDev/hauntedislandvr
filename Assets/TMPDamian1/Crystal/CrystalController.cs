﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrystalController : MonoBehaviour
{

    public GameObject ghostPrefab;
    public int width;
    public int depth;
    public int waveCounter;



    void GenerateWave()
    {
        for(int i = 0; i < waveCounter; i++)
        {
            Instantiate(ghostPrefab, new Vector3(Random.Range(0, width), 20, Random.Range(0, depth)), Quaternion.identity);
        }
    }


    void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.tag == "Player")
        {
            GenerateWave();
            Destroy(this.gameObject);
        }
    }
	

}
