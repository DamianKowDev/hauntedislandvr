﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GhostController : MonoBehaviour 
{

    public Transform player;
    public MeshFilter mesh;
    public float speed;
    [HideInInspector]
    public Vector3 direction;
    public GameObject fireBallPrefab;

    private Rigidbody rigidBody;
    private Vector3[] vertices;
    private float[,] heightMap;
    private bool isPlayerNearby;


    public float delayed;
    private float delay;
    public Transform fire;

	void Start ()
    {
        rigidBody = GetComponent<Rigidbody>();
     //   mesh =  GameObject.FindGameObjectWithTag("Mesh").GetComponent<MeshFilter>();
        vertices = mesh.sharedMesh.vertices;
        delay = delayed;
        
	}
	
	void Update ()
    {
        
        

        //this.gameObject.transform.position = new Vector3(x, y, z);

     

        
	}

    void FixedUpdate()
    {
        if (!isPlayerNearby)
        {
            rigidBody.constraints = RigidbodyConstraints.FreezeRotation;
            player = GameObject.FindGameObjectWithTag("Player").transform;
            Vector3 pos = this.transform.position;
            Vector3 playerPos = player.position;
            direction = (playerPos - pos);
            Vector3 movement = direction.normalized * speed* Time.deltaTime;
            //movement.y = 0;
            int x = (int)this.gameObject.transform.position.x;
            int z = (int)this.gameObject.transform.position.z;
            // int y = (int)vertices[z * x + x].y;
            //transform.position = new Vector3(transform.position.x, heightMap[z, x] * 16, transform.position.z);
            rigidBody.MovePosition(transform.position + movement);
            //transform.LookAt(player);
            //rigidBody.freezeRotation = false;
        }
        else
        {
            //rigidBody.freezeRotation = true;
            player = GameObject.FindGameObjectWithTag("Player").transform;
            transform.LookAt(player);
            rigidBody.constraints = RigidbodyConstraints.FreezeAll;
            delay -= Time.deltaTime;
            if (delay <= 0)
            {
                Instantiate(fireBallPrefab, fire.position, Quaternion.identity);
                delay = delayed;
            }
        }        
    }

    void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Player")
        {
            isPlayerNearby = true;
            Debug.Log("Player nearby");
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            isPlayerNearby = false;
            Debug.Log("Follow player");
        }
    }
}
