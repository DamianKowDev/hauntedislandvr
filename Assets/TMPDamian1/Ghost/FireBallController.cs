﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireBallController : MonoBehaviour 
{
    public GameObject ghost;
    private Rigidbody rb;
    public Transform player;

    private Vector3 direction;
    public  float thrust;
    public float ttl;

	void Start ()
    {
        //direction = ghost.GetComponent<GhostController>().direction;
        rb = GetComponent<Rigidbody>();
        
	}

    void Update()
    {
        ttl -= Time.deltaTime;
        if(ttl <= 0)
        {
            Destroy(this.gameObject);
        }
    }

    private bool a = true;
    void FixedUpdate()
    {
        try
        {
            if (a)
            {
                player = GameObject.FindGameObjectWithTag("Player").transform;
                a = false;
            }
            Vector3 pos = this.transform.position;
            Vector3 playerPos = player.position;
            direction = (playerPos - pos);
            Vector3 movement = direction.normalized * thrust * Time.deltaTime;
            //movement.y = 0;
            rb.MovePosition(transform.position + movement);
        }
        catch(Exception)
        {
        }
    }
	
    void OnCollisionEnter(Collision other)
    {
        if(other.gameObject.tag == "Player" || other.gameObject.tag == "Enemy")
        {
            EnemyHealth enemyHealth = other.gameObject.GetComponent<EnemyHealth>();
            if(enemyHealth != null)
            {
                enemyHealth.TakeDamage(5);
                Debug.Log("Take damage 5");
            }
            Destroy(this.gameObject);
        }
    }

}
