﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHealth : MonoBehaviour
{
    public int maxHealth = 100;
    public int currentHealth;

	void Start () 
    {
        currentHealth = maxHealth;	
	}

    void Update()
    {
        this.transform.LookAt(GameObject.FindGameObjectWithTag("Player").transform); ///
    }
	
    public void TakeDamage(int damage)
    {
        currentHealth -= damage;
        if(currentHealth <= 0)
        {
            GameController.ghostCounter--;
            Debug.Log("Enemy dead");
            Destroy(this.gameObject);
        }
    }
}