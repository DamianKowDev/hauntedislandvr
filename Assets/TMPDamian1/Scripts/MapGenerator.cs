﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapGenerator : MonoBehaviour
{

    public enum DrawMode{NoiseMap, ColourMap, Mesh};

    public DrawMode drawMode;

    public int mapWidth;
    public int mapHeight;
    public float noiseScale;

    public int octaves;
    [Range(0, 1)]
    public float persistance;
    public float lacunarity;

    public int seed;
    public Vector2 offset;

    public float meshHeightMultiplier;
    public AnimationCurve meshHeightMultiplierCurve;

    public bool autoUpdate;

    public TerrainType[] regions;

    private TerrainType[,] terrainTypes;

    private MapDisplay mapDisplay;

    public GameObject[] treesPrefab;
    public GameObject[] sandsPrefab;
    public GameObject cloudWithSnowPrefab;
    public GameObject cloudPrefab;
    public GameObject[] crystalsPrefab;
    public int crystalsCount;

    public bool generateEnvoirment;
    public bool generateClouds;
    public bool generateCrystals;
    public float[,] noiseMap;


    private int treeCount;
    public int treeCounter;
    void Start()
    {
		this.seed =  UnityEngine.Random.Range(0,10000);
        treeCount = 0;
        GenerateMap();
        
    }

    public void GenerateMap()
    {
         noiseMap = Noise.GenerateNoiseMap(mapWidth, mapHeight, seed, noiseScale, octaves, persistance, lacunarity, offset);

      

        Color[] colourMap = new Color[mapWidth * mapHeight];
        for(int y = 0; y < mapHeight; y++)
        {
            for(int x = 0; x < mapWidth; x++)
            {
                float currentHeight = noiseMap[y, x]; ///changes
                for(int i = 0; i < regions.Length; i++)
                {
                    if(currentHeight <= regions[i].height)
                    {
                        colourMap[y * mapWidth + x] = regions[i].colour;
                        break;
                    }
                }
            }
        }

       // GenerateTrees(mapHeight, mapWidth, noiseMap);


        MapDisplay display = FindObjectOfType<MapDisplay>();


      


        if(drawMode == DrawMode.NoiseMap)
        {
            display.DrawTexture(TextureGenerator.TextureFromHeightMap(noiseMap));
        }
        else if(drawMode == DrawMode.ColourMap)
        {
            display.DrawTexture(TextureGenerator.TextureFromColourMap(colourMap, mapWidth, mapHeight));
        }
        else if(drawMode == DrawMode.Mesh)
        {
            display.DrawMesh(MeshGenerator.GenerateTerrainMesh(noiseMap, meshHeightMultiplier, meshHeightMultiplierCurve), (TextureGenerator.TextureFromColourMap(colourMap, mapWidth, mapHeight)));
            try
            {
                if (generateEnvoirment)
                {
                    TreeGeneration(noiseMap, mapWidth, mapHeight, display.meshFilter, 0.65f, 0.7f, treesPrefab, sandsPrefab);
                }
                if(generateCrystals)
                {
                    GenerateCrystals(display.meshFilter);
                }
            }
            catch (Exception ex)
            {
                Debug.Log(ex);
            }
        }
    }

    private void GenerateClouds(MeshFilter meshFilter)
    {
        int cloudsCounter = 10;
         Vector3[] vertices = meshFilter.sharedMesh.vertices;
         GameObject clouds = new GameObject();
        for(int i = 0; i < cloudsCounter; i++)
        {
            int x = UnityEngine.Random.Range(0, mapWidth);
            int z = UnityEngine.Random.Range(0, mapHeight);
            int vertexIndex = x * mapWidth + z;

            GameObject cloud = Instantiate(cloudPrefab, vertices[vertexIndex], Quaternion.identity);
            cloud.transform.parent = clouds.transform;
        }
    }

    private void GenerateCrystals(MeshFilter meshFilter)
    {
        Vector3[] vertices = meshFilter.sharedMesh.vertices;
        GameObject crystals = new GameObject();
        for (int i = 0; i < crystalsCount; i++)
        {
            int x = UnityEngine.Random.Range(0, mapWidth);
            int z = UnityEngine.Random.Range(0, mapHeight);
            int vertexIndex = x * mapWidth + z;

            GameObject cloud = Instantiate(crystalsPrefab[UnityEngine.Random.Range(0, crystalsPrefab.Length)], vertices[vertexIndex], Quaternion.identity);
            cloud.transform.parent = crystals.transform;
        }
    }

    private void TreeGeneration(float[,] heightMap, int width, int depth, MeshFilter meshFilter, float min, float max, GameObject[] t, GameObject[] sands)
    {
        //Debug.Log("Tree generation start");
        Vector3[] vertices = meshFilter.sharedMesh.vertices;

        UnityEngine.Random.seed = seed;

        if (generateClouds)
        {
            GenerateClouds(meshFilter);
        }

        GameObject empty = new GameObject();

        for (int x = 0; x < depth; x++)
        {
            for (int z = 0; z < width; z++)
            {
                int vertexIndex = x * width + z;
                int percentChance = UnityEngine.Random.Range(0, 100);
                //Debug.Log(treeCount + " " + treeCounter);
                    if (heightMap[z, x] > 0.5f && heightMap[z, x] < 0.7f && percentChance > 98)
                    {
                        GameObject tree = Instantiate(t[UnityEngine.Random.Range(0, t.Length)], vertices[vertexIndex], Quaternion.identity);
                        tree.transform.parent = empty.transform;
                       
                    }
                    else if (heightMap[z, x] > 0.4f && heightMap[z, x] < 0.5f && percentChance == 88)
                    {
                        GameObject palm = Instantiate(sands[UnityEngine.Random.Range(0, sands.Length)], vertices[vertexIndex], Quaternion.identity);
                        palm.transform.parent = empty.transform;
                      
                    }
                    else if (heightMap[z, x] > 0.97f && heightMap[z, x] < 1f && percentChance > 0)
                    {
                        if (generateClouds)
                        {
                            GameObject cloud = Instantiate(cloudWithSnowPrefab, vertices[vertexIndex], Quaternion.identity);
                            cloud.transform.parent = empty.transform;
                        }
                    }
            }
        }
    }

    string[,] str;

    private TerrainType[,] ChosenTerrainTypes(float[,] heightMap, int width, int depth) ////
    {
        TerrainType[,] tmpTerrainTypes = new TerrainType[depth, width];
        str = new string[depth, width];
        for(int zIndex = 0; zIndex < depth; zIndex++)
        {
            for(int xIndex = 0; xIndex < width; xIndex++)
            {
                foreach(TerrainType t in regions)
                {
                    if(heightMap[zIndex, xIndex] < t.height)
                    {
                        tmpTerrainTypes[zIndex, xIndex] = t;
                        str[zIndex, xIndex] = t.name;
                        break;
                    }
                }
            }
           
        }

        return tmpTerrainTypes;
    }

    private void GenerateTrees(int depth, int width, float[,] heightMap)
    {
        mapDisplay = GetComponent<MapDisplay>();


        float[,] treeMap = heightMap;

        Vector3[] meshVertices = mapDisplay.meshFilter.sharedMesh.vertices;
        int tileDepthInVertices = (int)Mathf.Sqrt(meshVertices.Length);
        int tileWidthInVertices = tileDepthInVertices;

        float distanceBetweenVertices = 1700f / (float)tileDepthInVertices;


        float topLeftX = (width - 1) / -2f;
        float topLeftZ = (depth - 1) / 2f;

        terrainTypes = ChosenTerrainTypes(heightMap, mapWidth, mapHeight); ////

        for (int zIndex = 0; zIndex < depth; zIndex++)
        {
            for (int xIndex = 0; xIndex < width; xIndex++)
            {
                int vertexIndex = zIndex * width + xIndex;
                if (str[zIndex,xIndex] == "Grass1" || str[zIndex, xIndex] == "Grass2")
                {
                    float treeValue = treeMap[zIndex, xIndex];

                    float radius = 1f;

                    int neighborZBegin = (int)Mathf.Max(0, zIndex - radius); //this.neighborRadius[biome.index]
                    int neighborZEnd = (int)Mathf.Min(depth - 1, zIndex + radius);
                    int neighborXBegin = (int)Mathf.Max(0, xIndex - radius);
                    int neighborXEnd = (int)Mathf.Min(width - 1, xIndex + radius);
                    float maxValue = 0f;
                    for (int neighborZ = neighborZBegin; neighborZ <= neighborZEnd; neighborZ++)
                    {
                        for (int neighborX = neighborXBegin; neighborX <= neighborXEnd; neighborX++)
                        {
                            float neighborValue = treeMap[neighborZ, neighborX];
                            if (neighborValue >= maxValue)
                            {
                                maxValue = neighborValue;
                            }
                        }
                    }

                    if (treeValue == maxValue)
                    {
                        Vector3 treePosition = new Vector3((xIndex + topLeftX) , meshVertices[vertexIndex].y, (topLeftZ - zIndex));
                       // Vector3 treePosition = new Vector3(meshVertices[vertexIndex].x*10, meshVertices[vertexIndex].y *10, meshVertices[vertexIndex].z*10);
                      // GameObject tree = Instantiate(treePrefab, treePosition, Quaternion.identity) as GameObject;

                    }
                }
            }
        }

    }


    void OnValidate()
    {
        if (mapWidth < 1)
        {
            mapWidth = 1;
        }
        if (mapHeight < 1)
        {
            mapHeight = 1;
        }
        if (lacunarity < 1)
        {
            lacunarity = 1;
        }
        if (octaves < 0)
        {
            octaves = 0;
        }
    }


    [System.Serializable]
    public class TerrainType
    {
        public string name;
        public float height;
        public Color colour;
    }

}

