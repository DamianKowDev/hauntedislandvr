﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAttack : MonoBehaviour {


    public PlayerAttackMode playerAttackMode;
    public Transform spellPlace;
    public GameObject spellPrefab;
    public float delay;
    private float timer;



    void Start () 
    {
        playerAttackMode = PlayerAttackMode.FIREBALL;
        timer = delay;
	}
	
	void Update ()
    {
        timer -= Time.deltaTime;
      //if(/*Input.GetMouseButton(0) &&*/ playerAttackMode == PlayerAttackMode.FIREBALL && timer <= 0)
       /* {
            timer = delay;
            Debug.Log("Cast spell");
            GameObject spell = Instantiate(spellPrefab, spellPlace.position, Quaternion.identity);

            spell.gameObject.GetComponent<PlayerSpell>().CastSpell(Camera.main.transform.forward);
        }*/
	}

    public enum PlayerAttackMode { BOW, FIREBALL };

    public void CastSpell()
    {
        Debug.Log("Casting spell");
        if (/*Input.GetMouseButton(0) &&*/ playerAttackMode == PlayerAttackMode.FIREBALL && timer <= 0)
        {
            timer = delay;
            Debug.Log("Cast spell");
            GameObject spell = Instantiate(spellPrefab, spellPlace.position, Quaternion.identity);

            spell.gameObject.GetComponent<PlayerSpell>().CastSpell(Camera.main.transform.forward);
        }
    }
}
