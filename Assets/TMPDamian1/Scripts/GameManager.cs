﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour 
{

    private int crystalsCount;
    private int ghostsCount;

    void Start()
    {
        crystalsCount = GameObject.FindGameObjectWithTag("MapGenerator").gameObject.GetComponent<MapGenerator>().crystalsCount;
        ghostsCount = GameObject.FindGameObjectWithTag("Crystal").gameObject.GetComponent<CrystalController>().waveCounter;
    }

}
