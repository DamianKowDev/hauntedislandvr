﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSpell : MonoBehaviour {


    private Rigidbody rb;
    public float strength;
    public int damage;
    public float ttl;

	void Start () 
    {
        rb = GetComponent<Rigidbody>();
	}


    void Update()
    {
        ttl -= Time.deltaTime;
        if (ttl <= 0)
        {
            Destroy(this.gameObject);
        }
    }


    public void CastSpell(Vector3 direction)
    {
        rb = GetComponent<Rigidbody>();
        if(rb != null)
        {
            rb.velocity = (direction * strength);
        }
        else
        {
            Debug.Log("Rigibody spell is null");
        }
        
    }

    void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Enemy")
        {
            EnemyHealth enemyHealth = other.gameObject.GetComponent<EnemyHealth>();
            if(enemyHealth != null)
            {
                enemyHealth.TakeDamage(damage);
                Destroy(this.gameObject);
                Debug.Log("Przeciwnik otrzymał " + damage + " obrażeń");
            }
        }
    }
	
}
