﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TreeGeneration : MonoBehaviour
{

    public GameObject treePrefab;



    public static void TreeGen(float[,] heightMap, int width, int depth, MeshFilter meshFilter, float min, float max, GameObject t)
    {
        Debug.Log("Tree generation start");
        Vector3[] vertices = meshFilter.sharedMesh.vertices;

        GameObject empty = new GameObject();

        for(int x = 0; x < depth; x++)
        {
            for(int z = 0; z < width; z++)
            {
                int vertexIndex = x * width + z; //x z
                if(heightMap[z,x] > min && heightMap[z,x] < max)
                {
                    GameObject tree = Instantiate(t, vertices[vertexIndex], Quaternion.identity);
                    tree.transform.parent = empty.transform;
                }
            }
        }
    }

    

	
}
