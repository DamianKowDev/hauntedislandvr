﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapDisplay : MonoBehaviour 
{
    public Renderer textureRender;
    public MeshFilter meshFilter;
    public MeshRenderer meshRenderer;
    public MeshCollider meshCollider;
    public GameObject mesh;


 
    public void DrawTexture(Texture2D texture)
    {
        textureRender.sharedMaterial.mainTexture = texture; //in editor mode, material in runtime only
        textureRender.transform.localScale = new Vector3(texture.width, 1, texture.height);
    }

    public void DrawMesh(MeshData meshData, Texture2D texture)
    {

        meshFilter.sharedMesh = meshData.CreateMesh();

       // mesh.transform.localScale = new Vector3(1f, 1f, -1f);
       // mesh.transform.Rotate(0, 90, 0);

		this.meshFilter.sharedMesh.RecalculateBounds ();
		this.meshFilter.sharedMesh.RecalculateNormals ();
		this.meshCollider.sharedMesh = this.meshFilter.sharedMesh;

        meshRenderer.sharedMaterial.mainTexture = texture;
    }
}
