﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControllerGrabObject : MonoBehaviour {

	private SteamVR_TrackedObject trackedObj;
	private GameObject collidingObject; 
	private GameObject objectInHand; 

	private SteamVR_Controller.Device Controller
	{
		get { return SteamVR_Controller.Input((int)trackedObj.index); }
	}

	void Awake()
	{
		trackedObj = GetComponent<SteamVR_TrackedObject>();
	}

	private void SetCollidingObject(Collider col)
	{
		// 1
		if (collidingObject || !col.GetComponent<Rigidbody>())
		{
			return;
		}
		// 2
		collidingObject = col.gameObject;
	}

	// 1
	public void OnTriggerEnter(Collider other)
	{
		SetCollidingObject(other);
	}

	// 2
	public void OnTriggerStay(Collider other)
	{
		SetCollidingObject(other);
	}

	// 3
	public void OnTriggerExit(Collider other)
	{
		if (!collidingObject)
		{
			return;
		}

		collidingObject = null;
	}

	private void GrabObject()
	{
		// 1
		objectInHand = collidingObject;
		collidingObject = null;
		// 2
		var joint = AddFixedJoint();
		joint.connectedBody = objectInHand.GetComponent<Rigidbody>();
	}

	// 3
	private FixedJoint AddFixedJoint()
	{
		FixedJoint fx = gameObject.AddComponent<FixedJoint>();
		fx.breakForce = 20000;
		fx.breakTorque = 20000;
		return fx;
	}

	private void ReleaseObject()
	{
		// 1
		if (GetComponent<FixedJoint>())
		{
			// 2
			GetComponent<FixedJoint>().connectedBody = null;
			Destroy(GetComponent<FixedJoint>());
			// 3
			objectInHand.GetComponent<Rigidbody>().velocity = Controller.velocity;
			objectInHand.GetComponent<Rigidbody>().angularVelocity = Controller.angularVelocity;
		}
		// 4
		objectInHand = null;
	}


	public GameObject fireBallPrefab;


	public void CastSpell(Vector3 direction, Rigidbody rb)
	{
		rb = GetComponent<Rigidbody>();
		if (rb != null)
		{
			rb.velocity = (direction * 50);
		}
		else
		{
			Debug.Log("Rigibody spell is null");
		}

	}

	public static bool canAttack = true;
	public GameObject lighting;

    public float timer;
    public float delayed;

    void Start()
    {
        delayed = timer;
    }

    public int controllerIndexLeft = 1;
    public int controllerIndexRight = 2;

	void Update () {

        delayed -= Time.deltaTime;
		// 1
		if (Controller.GetHairTrigger() && canAttack && Controller.index == controllerIndexRight && delayed <= 0)
		{
            delayed = timer;
			Debug.Log("Fired");
			GameObject spell = Instantiate(fireBallPrefab, this.transform.position, Quaternion.identity);

			CastSpell(transform.forward, spell.GetComponent<Rigidbody>());

			spell.gameObject.GetComponent<PlayerSpell>().CastSpell(transform.forward);

			
			/*if (collidingObject)
			{
				GrabObject();
			}*/
		}

		if(Controller.GetHairTrigger() && canAttack && Controller.index == controllerIndexLeft)
		{
			Debug.Log("Piorunek szczela");
			lighting.SetActive(true);
            int layerMask = 1 << 8;
            RaycastHit _hit;
            if (Physics.Raycast(transform.position, transform.forward * 50f, out _hit, 50f))
            {
                Debug.LogWarning("Hit " + _hit.collider.name);
                if(_hit.collider.name == "KacperekBeta(Clone)" && delayed <= 0)
                {
                     delayed = timer;
                    _hit.collider.SendMessage("TakeDamage", 10);
                }
            }
        }
		else if(Controller.GetHairTriggerUp() && Controller.index == controllerIndexLeft)
		{
			lighting.SetActive(false);
		}

		// 2
		if (Controller.GetHairTriggerUp())
		{
			if (objectInHand)
			{
				ReleaseObject();
			}
		}

	}
}
