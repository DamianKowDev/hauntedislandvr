﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserPointer : MonoBehaviour {

	private SteamVR_TrackedObject trackedObj;
	public GameObject laserPrefab;
	private GameObject laser;
	private Transform laserTransform;
	private Vector3 hitPoint;

	public Transform cameraRigTransform; 
	// 2
	public GameObject teleportReticlePrefab;
	// 3
	private GameObject reticle;
	// 4
	private Transform teleportReticleTransform; 
	// 5
	public Transform headTransform; 
	// 6
	public Vector3 teleportReticleOffset; 
	// 7
	public LayerMask teleportMask; 
	// 8
	private bool shouldTeleport; 

	private SteamVR_Controller.Device Controller
	{
		get { return SteamVR_Controller.Input((int)trackedObj.index); }
	}

	void Awake()
	{
		trackedObj = GetComponent<SteamVR_TrackedObject>();
	}

	private void ShowLaser(RaycastHit hit)
	{
		laser.SetActive(true);
		laserTransform.position = Vector3.Lerp(trackedObj.transform.position, hitPoint, .5f);
		laserTransform.LookAt(hitPoint); 
		laserTransform.localScale = new Vector3(laserTransform.localScale.x, laserTransform.localScale.y,
			hit.distance);
	}

	private void Teleport()
	{
		// 1
		shouldTeleport = false;
		// 2
		reticle.SetActive(false);
		// 3
		Vector3 difference = cameraRigTransform.position - headTransform.position;
		// 4
		difference.y = 0;
		// 5
		cameraRigTransform.position = hitPoint + difference;
        Time.timeScale = 1f;
        ControllerGrabObject.canAttack = true;
    }



	void Start () {
		laser = Instantiate(laserPrefab);
		laserTransform = laser.transform;

		// 1
		reticle = Instantiate(teleportReticlePrefab);
		// 2
		teleportReticleTransform = reticle.transform;
	}

    

	void Update () {

        int controllerTeleportIndex = GetComponent<ControllerGrabObject>().controllerIndexRight;

		if (Controller.GetPress(SteamVR_Controller.ButtonMask.Touchpad) && Controller.index == controllerTeleportIndex)
		{
            Time.timeScale = 0.2f;
            ControllerGrabObject.canAttack = false;
			RaycastHit hit;

			if (Physics.Raycast(trackedObj.transform.position, transform.forward, out hit, 100, teleportMask))
			{
				hitPoint = hit.point;
				ShowLaser(hit);

				// 1
				reticle.SetActive(true);
				// 2
				teleportReticleTransform.position = hitPoint + teleportReticleOffset;
				// 3
				shouldTeleport = true;
			}
		}
		else 
		{
            laser.SetActive(false);
			reticle.SetActive(false);
		}

		if (Controller.GetPressUp(SteamVR_Controller.ButtonMask.Touchpad) && shouldTeleport)
		{
			Teleport();
		}
	}
}
